<?php

namespace W7\Validate\Providers\Think;

use Itwmw\Validation\Factory;
use think\facade\Config;
use think\Service;
use W7\Validate\Support\Storage\ValidateConfig;

class ValidateService extends Service
{
    public function register()
    {
        $factory = new Factory(null, str_replace('-', '_', Config::get('lang.default_lang')));
        $factory->setPresenceVerifier(new PresenceVerifier());
        ValidateConfig::instance()->setFactory($factory);
    }
}
