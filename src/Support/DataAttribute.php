<?php

namespace W7\Validate\Support;

class DataAttribute
{
    /**
     * Whether to delete the field in the check data.
     */
    public bool $deleteField = false;
}
