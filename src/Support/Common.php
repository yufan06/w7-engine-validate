<?php

namespace W7\Validate\Support;

class Common
{
    /**
     * Get the name and parameters of the rule.
     *
     * @param string $value   Complete Rules
     * @param bool   $parsing Whether to parse parameters, default is false
     */
    public static function getKeyAndParam(string $value, bool $parsing = false): array
    {
        $param = null;
        if (str_contains($value, ':')) {
            $arg = explode(':', $value, 2);
            $key = $arg[0];
            if (count($arg) >= 2) {
                $param = $arg[1];
            }
        } else {
            $key = $value;
        }

        if ($parsing && !is_null($param)) {
            $param = explode(',', $param);
        }
        return [$key, $param];
    }

    /**
     * Name of the generated error message.
     */
    public static function makeMessageName(string $fieldName, string $rule): string
    {
        if (str_contains($rule, ':')) {
            $rule = substr($rule, 0, strpos($rule, ':'));
        }
        return $fieldName . '.' . $rule;
    }

    /**
     * Get rules for a given field and populate non-existent fields.
     */
    public static function getRulesAndFill(array $rules, array $fields): array
    {
        $rules    = array_intersect_key($rules, array_flip($fields));
        $notExist = array_diff($fields, array_keys($rules));
        return array_merge($rules, array_fill_keys($notExist, []));
    }
}
